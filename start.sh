#!/bin/bash

/var/www/quake/virtualenv/bin/gunicorn_django -b localhost:9001 -D --log-file /var/www/quake/logs/quake_web.log -k gevent --pythonpath /var/www/quake/quake_web/ --settings quake_web.settings
