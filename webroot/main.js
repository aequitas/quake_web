$(document).ready(function(){
  pi = new PlayerInfo(10)
  mi = new MapInfo(30)
});

gametypes = ['DM','?','TDM','?','CTF','?','?','?']

PlayerInfo = function(interval){
  var self = this

  this.update = function(){
    if (self.request && self.request.state() == 'pending')
      return
    self.request = $.getJSON('/players/live', self.putdata);
  }

  this.putdata = function(playerinfo){
    playerinfo['time'] = Math.round( ( new Date() - new Date(playerinfo['update_time']*1000))/1000 ) + 's behind'
    $('#playerinfo').html($('.templates .playerinfo').clone()).autoRender(playerinfo);
  }

  this.timer = setInterval(this.update, interval*1000);
  this.update()
}

MapInfo = function(interval){
  var self = this

  this.update = function(){
    if (self.request && self.request.state() == 'pending')
      return
    self.request = $.getJSON('/maps/live', self.putdata);
  }

  this.putdata = function(data){
    data['time'] = Math.round( ( new Date() - new Date(data['update_time']*1000))/1000 ) + 's behind'
    data['current_map_img'] = '/media/images/levelshot/' + data['current_map'] + '.png';
    data['next_map_img'] = '/media/images/levelshot/' + data['next_map'] + '.png';
    data['current_gametype'] = gametypes[data['current_gametype']];
    data['next_gametype'] = gametypes[data['next_gametype']];
    $('#mapinfo').html($('.templates .mapinfo').clone()).autoRender(data)
  }

  this.timer = setInterval(this.update, interval*1000);
  this.update()
}
