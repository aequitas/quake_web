from glob import glob
import Image
import os
import shutil

for picture in glob("/var/www/quake/baseoa/levelshots/*"):
    mapname = picture.split('/')[-1][:-4]
    outfile = 'maps/%s.png' % mapname

    print picture, mapname, outfile

    try:
        print "Converting: ", picture
        Image.open(picture).save(outfile)
    except IOError:
        print "    * Conversion failed for this file."
