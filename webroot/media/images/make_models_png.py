from glob import glob
import Image
import os
import shutil

for picture in glob("/var/www/quake/baseoa/models/players/*/icon_*.tga"):
    model = picture.split('/')[-2]
    submodel = picture.split('_')[-1][:-4]

    if submodel == 'default':
        submodel = ''
    else:
        submodel = '_%s' % submodel

    outfile = 'playermodel/%s%s.png' % (model, submodel)

    print picture, model, submodel, outfile

    try:
        print "Converting: ", picture
        Image.open(picture).save(outfile)
    except IOError:
        print "    * Conversion failed for this file."
