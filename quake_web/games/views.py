from maps.models import Map

import logbook
log = logbook.Logger()

from django.utils import simplejson
from django.http import HttpResponse

game = dict()

game.update(Map.mapinfo())

def gameinfo(request):
  return HttpResponse(simplejson.dumps(game), mimetype='application/json')
