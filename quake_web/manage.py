#!/var/www/quake/virtualenv/bin/python
import os
import sys

import logbook

log = logbook.Logger()

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "quake_web.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
