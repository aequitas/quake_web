from django.test import TestCase
from models import *


class PlayerTest(TestCase):
    def setUp(self):
        p = Player(name='test')
        p.save()

    def test_adding_player(self):
        self.assertTrue(Player.objects.get(name='test'))

    def test_setting_player_model(self):
        p = Player.objects.get(name='test')
        p.playermodel.add(PlayerModel.objects.get_or_create(name='testmodel')[0])
        p.save()
        self.assertTrue(p.playermodel.get().name == 'testmodel')

    def test_playermodel_image(self):
        pl = Player.objects.get()
        pl.playermodel.add(PlayerModel.objects.get_or_create(name='testmodel')[0])
        pl.save()
        import pdb; pdb.set_trace()
