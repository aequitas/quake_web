from django.conf import settings

from django.db import models
from media.models import Image

from tasks import register_task
import gevent

import logbook
log = logbook.Logger()

import time, datetime

from pyquake3 import Administrator

class GUID(models.Model):
  guid = models.CharField(max_length=100, primary_key=True)
  
  def __unicode__(self):
    return self.guid


class PlayerModel(models.Model):
  name = models.CharField(max_length=100, primary_key=True)
  image = models.ManyToManyField(Image)
          
  def save(self, *args, **kwargs):
      log.debug('saving player model %s' % self.name)
      super(PlayerModel, self).save(*args, **kwargs)

      if not self.image.all():
        log.debug('adding image')
        self.image.add(Image.objects.get_or_create(name=self.name, type='playermodel')[0])
        self.save()

  def __unicode__(self):
    return self.name

class LivePlayerInfo(object):
  online_players = list()
  players = dict()
  update_time = 0

  a = Administrator(settings.QUAKEHOST, settings.QUAKEPORT, settings.QUAKERCONPASSWD)

  @classmethod
  def task_playerstats(cls):
    while True:
      cls.update()
      gevent.sleep(5)

  @classmethod
  def update(cls):
    log.debug('updating player stats')
    try:
      # update players, name, frags, ping,
      cls.a.update()    
      if cls.a.players:
        # update players, slot, frags, ping, name, lastmsg, address, qport, rate
        cls.a.rcon_update()
        # update guid and player vars, requires rcon_update for ip/rate
        cls.a.rcon_dumpuser_all()
    except:
      log.exception()
      log.error('update failed')
      cls.update_time = -2
      return

    online_players = list()
    for playerinfo in cls.a.players:
      if not playerinfo.name in cls.online_players:
        log.debug('new player came online, getting more info')
        gevent.spawn(cls.hello_player, playerinfo)
      else:
        log.debug('player %s online' % playerinfo.name)
        gevent.spawn(cls.update_player, playerinfo)

      online_players.append(playerinfo.name)

    # update new online players
    cls.online_players = online_players

    # removename offline players from info list
    for playername in cls.players.keys():
      if not playername in cls.online_players:
        del cls.players[playername]

    cls.update_time = time.time()
    log.debug('update finished')

  @classmethod
  def hello_player(cls, playerinfo):
    log.debug('new player, info: %s' % playerinfo)
    
    (p, new) = Player.objects.get_or_create(name=playerinfo.name)
    if new:
      log.debug('New database entry created for player %s' % playerinfo.name)

    p.update_last_online()

    try:
      # get player model
      model = playerinfo.variables['model']
    except:
      log.exception()
      model = None

    if model and not p.playermodel == model:
      log.debug('setting model %s for player %s' % (model, playerinfo.name))
      p.set_model(model)

    try:
      guid = playerinfo.guid
    except:
      log.exception()
      guid = None

    if guid and len(p.guids.filter(guid=guid)) == 0:
      log.info('adding new guid %s for player %s' % (guid, playerinfo.name))
      p.guids.add(GUID.objects.get_or_create(guid=guid)[0])

    p.save()

    # also update player live stats
    cls.update_player(playerinfo)

  @classmethod
  def update_player(cls, playerinfo):
    log.debug('update player, info: %s' % playerinfo)

    # fields: ['address', 'frags', 'guid', 'lastmsg', 'name', 'ping', 'qport', 'rate', 'slot', 'variables']

    # variables : {'teamtask': '0', 'snaps': '20', 'cl_anonymous': '0', 'cg_delag': '1', 'name': '-[aequitas]-', 'handicap': '100', 'ip': '82.161.93.152', 'team_headmodel': '*james', 'cl_voip': '1', 'teamoverlay': '1', 'sex': 'male', 'color1': '7', 'cl_guid': '8108800C535D099C1E2B15FAC1B55F04', 'rate': '25000', 'color2': '4', 'cg_scorePlums': '1', 'model': 'sarge', 'cg_predictItems': '1', 'headmodel': 'sarge', 'cg_cmdTimeNudge': '0', 'team_model': 'james'}

    fields = ['name', 'frags']
    variables = ['model', 'teamtask','team_headmodel', 'color1', 'color2']

    cls.players[playerinfo.name] = dict([(field, getattr(playerinfo, field, '')) for field in fields])
    cls.players[playerinfo.name].update(dict([(var, getattr(playerinfo, 'variables', []).get(var)) for var in variables]))
    cls.players[playerinfo.name].update({
      'playermodel': PlayerModel.objects.get(name=cls.players[playerinfo.name]['model']).image.all()[0].get_url(),
      #'team': 'red' if '*' in cls.players[playerinfo.name]['team_headmodel'] else 'red',
    })

  @classmethod
  def bye_player(cls, playername):
    log.debug('left player, name: %s' % playername)


class Player(models.Model, LivePlayerInfo):
  name = models.CharField(max_length=100) #, TODO primary_key=True)
  realname = models.CharField(max_length=100, null=True, blank=True)
  guids = models.ManyToManyField(GUID, null=True, blank=True)
  created = models.DateTimeField(auto_now_add=True)
  last_online = models.DateTimeField(null=True,blank=True)
  playermodel = models.ManyToManyField(PlayerModel, null=True, blank=True)

  def update_last_online(self):
    self.last_online = datetime.datetime.now()
    self.save()
  
  def set_model(self, model):
    if not self.playermodel.filter(name=model):
      log.debug('setting model for player %s to %s' % (self,model))
      self.playermodel.add(PlayerModel.objects.get_or_create(name=model)[0])
      self.save()

  def __unicode__(self):
    return self.name

register_task(Player.task_playerstats)
