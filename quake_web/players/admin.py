from django.contrib import admin
from models import *

admin.site.register(GUID, admin.ModelAdmin)
admin.site.register(Player, admin.ModelAdmin)
admin.site.register(PlayerModel, admin.ModelAdmin)
