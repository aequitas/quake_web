from django.utils import simplejson
from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from models import Player
import time

class ListView(ListView):
    model = Player

class DetailView(DetailView):
    model = Player

def thatsme(request, pk):
    p = Player.objects.get(id=pk)
    request.session['current_player'] = p.name

    realname = request.REQUEST.get('realname', '')
    if realname:
        p.realname = realname
        p.save()

    data = 'so you are player %s, <a href="/players/list">back</a>' % request.session['current_player']

    return HttpResponse(data)

def live(request):
    data = {
        'players':Player.players.values(),
        'update_time': int(Player.update_time),
    }
    return HttpResponse(simplejson.dumps(data), mimetype='application/json')
