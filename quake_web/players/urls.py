from django.conf.urls import patterns, url

from models import Player
import views

urlpatterns = patterns('',
    url(r'^list/?$', views.ListView.as_view(queryset=Player.objects.order_by('-last_online')), name="players"),
    url(r'^detail/(?P<pk>[0-9]*)$', views.DetailView.as_view(), name='detail'),
    url(r'^thatsme/(?P<pk>.*)$', views.thatsme, name='thatsme'),
    url(r'^detail/(?P<slug>.*)$', views.DetailView.as_view(slug_field='name'), name='by_name'),
    url(r'^live$', views.live),
)
