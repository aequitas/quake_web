from django.utils import simplejson
from django.http import HttpResponse
from django.views.generic import ListView, DetailView, TemplateView
from models import Map
from django.db.models import Sum

class MapListView(ListView):
    model = Map
    queryset = Map.objects.annotate(votes=Sum('raw_votes__vote'))

class DetailView(DetailView):
    model = Map


class ConfigView(ListView):
    def get(self, *args, **kwargs):
        response = ListView.get(self,*args, **kwargs)
        response['Content-Type'] = 'text/plain'
        return response
    

class MapCfgView(ListView):
    template_name = "maps/mapcfg.cfg"
    model = Map

    def get(self, *args, **kwargs):
        output = self.mapcfg(self.get_queryset())
        return HttpResponse(output,mimetype='text/plain')
        
    def mapcfg(self, maps):
        output = ''
        count = 0
        for map in maps:
            mapname = map.name
            gametypes = map.gametypes.all()

            if len(gametypes):
                gametype = gametypes[0].id
            else:
                gametype = 0

            count += 1
            next = count + 1
            if next > len(maps):
                next = 1

            output += 'set map%s "set g_gametype %s; map %s; set nextmap vstr map%s;"\n' % (
                count, gametype, mapname, next
            )

        return HttpResponse(output,mimetype='text/plain')

def live(request):
    data = Map.mapinfo()
    return HttpResponse(simplejson.dumps(data), mimetype='application/json')

def vote(request, mapname, vote, player):
    m = Map.objects.get(name=mapname)
    if vote == 'up':
        vote = 1
    else:
        vote = -1

    if m.vote(vote, player):
        data = { 
            'mapid' : m.pk,
            'player' : player,
            'votes' : m.vote_sum()['vote__sum'],
        }
        return HttpResponse(simplejson.dumps(data), mimetype='text/plain')

def mapcfg(request):
    output = ''
    count = 0
    maps = Map.objects.all()
    for map in maps:
        mapname = map.name
        gametypes = map.gametypes.all()

        if len(gametypes):
            gametype = gametypes[0].id
        else:
            gametype = 0

        count += 1
        next = count + 1
        if next > len(maps):
            next = 1

        output += 'set map%s "set g_gametype %s; map %s; set nextmap vstr map%s;"\n' % (
            count, gametype, mapname, next
        )

    return HttpResponse(output,mimetype='text/plain')
