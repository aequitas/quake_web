from django.conf.urls import patterns, url

from maps.views import * 
from maps.models import *
import views

maps = Map.objects.annotate(votes=Sum('raw_votes__vote')).order_by('exclude')

urlpatterns = patterns('',
    url(r'^list/map.cfg$', ConfigView.as_view(queryset=maps.filter(exclude=False), template_name="maps/mapcfg.cfg"), name="maps_config"),
    url(r'^list/?$', ListView.as_view(queryset=maps), name="maps"),
    url(r'^favorite/map.cfg$', ConfigView.as_view(queryset=maps.filter(votes__gt=0,exclude=False), template_name="maps/mapcfg.cfg"), name="maps_config"),
    url(r'^favorite/?$', ListView.as_view(queryset=maps.filter(votes__gt=0).order_by('-votes'), model=Map), name="maps"),
    url(r'^last/map.cfg$', ConfigView.as_view(queryset=maps.filter(last_played__isnull=False,exclude=False).order_by('-last_played')[:10], template_name="maps/mapcfg.cfg"), name="maps_config"),
    url(r'^last/?$', ListView.as_view(queryset=maps.filter(last_played__isnull=False).order_by('-last_played')[:10], model=Map), name="maps"),
    url(r'^detail/(?P<pk>.*)$', DetailView.as_view(), name="map"),
    url(r'^maps.cfg', mapcfg),
    url(r'^live$', views.live),
    url(r'^(?P<mapname>.*)/vote/(?P<vote>.*)/(?P<player>.*)$', 'maps.views.vote'),
)

#TODO https://docs.djangoproject.com/en/dev/topics/http/urls/#the-view-prefix
