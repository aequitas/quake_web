from django.test import TestCase
from models import *

import gevent

class MapTests(TestCase):
    def test_add_map(self):
        map = Map(name="agressor")
        map.save()
        gevent.sleep(0)

        for img in map.images.all():
            print img.url()
