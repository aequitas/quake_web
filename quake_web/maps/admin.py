from django.contrib import admin
from models import *

admin.site.register(Vote, admin.ModelAdmin)
admin.site.register(Map, admin.ModelAdmin)
admin.site.register(GameType, admin.ModelAdmin)
