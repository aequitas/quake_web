from django.db import models
from players.models import Player
from media.models import Image
from django.db.models import Sum

from tasks import register_task

from django.conf import settings

from pyquake3 import Administrator

import gevent
import time, datetime
import re

import logbook
log = logbook.Logger()
  
class GameType(models.Model):
  id = models.IntegerField(primary_key=True)
  name = models.CharField(max_length=100)

  def __unicode__(self):
    return self.name

class LiveMapStats(object):
  current_map = ''
  current_gametype = ''
  next_map = ''
  next_gametype = ''
  update_time = 0

  a = Administrator(settings.QUAKEHOST, settings.QUAKEPORT, settings.QUAKERCONPASSWD)

  @classmethod
  def task_mapstats(cls):
    while True:
      cls.update()
      gevent.sleep(10)

  @classmethod
  def update(cls):
    log.debug('updating map stats')
    try:
      # update map
      cls.a.update()    
      
      # ('statusResponse', '\\sv_floodProtect\\1\\sv_maxPing\\0\\sv_minPing\\0\\sv_maxRate\\0\\sv_minRate\\0\\sv_hostname\\noname\\dmflags\\0\\fraglimit\\20\\timelimit\\0\\sv_maxclients\\8\\g_maxGameClients\\0\\videoflags\\7\\capturelimit\\8\\g_doWarmup\\0\\g_allowVote\\1\\g_voteGametypes\\/0/1/3/4/5/6/7/8/9/10/11/12/\\g_voteMaxTimelimit\\1000\\g_voteMinTimelimit\\0\\g_voteMaxFraglimit\\0\\g_voteMinFraglimit\\0\\g_delagHitscan\\0\\elimination_roundtime\\120\\g_lms_mode\\0\\version\\ioq3+oa 1.36_SVN1910M linux-i386 Dec 25 2011\\protocol\\71\\g_gametype\\0\\mapname\\aggressor\\sv_privateClients\\0\\sv_allowDownload\\0\\bot_minplayers\\0\\gamename\\baseoa\\elimflags\\0\\voteflags\\767\\g_needpass\\0\\g_obeliskRespawnDelay\\10\\g_enableDust\\0\\g_enableBreath\\0\\g_rockets\\0\\g_instantgib\\0\\g_altExcellent\\0\\g_timestamp\\2012-12-15 15:56:34\n')

    except:
      log.exception()
      log.error('update failed')
      cls.update_time = -2
      return

    mapname = cls.a.variables['mapname']
    if not cls.current_map == mapname:
      log.debug('map changed to %s' % mapname)
      (m, new) = Map.objects.get_or_create(name=mapname)
      m.update_last_played()
      gevent.spawn(cls.get_nextmap)
  
      cls.current_map = mapname
      cls.current_gametype = cls.a.variables['g_gametype']

    cls.update_time = time.time()
    log.debug('update finished')

  @classmethod
  def mapinfo(cls):
    return {
      'update_time': int(cls.update_time),
      'current_map': cls.current_map,
      'current_gametype': cls.current_gametype,
      'next_map': cls.next_map,
      'next_gametype': cls.next_gametype,
    }

  @classmethod
  def get_nextmap(cls):
    log.info('starting get_nextmap')
    a = Administrator(settings.QUAKEHOST, settings.QUAKEPORT, settings.QUAKERCONPASSWD)

    nextmapvar = a.rcon_command('print nextmap')[1]
    log.debug('nextmapvar: %s' % nextmapvar)

    nextmapvar = re.findall('vstr ([a-zA-Z0-9-_]+)', nextmapvar)[0]
    log.debug('nextmapvar: %s' % nextmapvar)

    nextmapstring = a.rcon_command('print %s' % nextmapvar)[1]
    log.debug('nextmapstring: %s' % nextmapstring)

    nextmap = re.findall('map ([a-zA-Z0-9-_]+)', nextmapstring)[0]
    nextgametype = re.findall('gametype ([a-zA-Z0-9-_]+)', nextmapstring)[0]
    log.debug('nextmap: %s, nextgametype: %s' % (nextmap, nextgametype))

    cls.next_gametype = nextgametype
    cls.next_map = nextmap 
    log.info('finished get_nextmap')
        

class Map(models.Model, LiveMapStats):
  name = models.CharField(max_length=100, primary_key=True, help_text="system name of the map, example: agressor")
  nicename = models.CharField(max_length=100, blank=True, null=True, help_text="Nice name for the map, example: Agressor")
  #votes = models.ManyToManyField(Vote, blank=True, null=True, help_text="Up/Down votes for the map")
  gametypes = models.ManyToManyField(GameType, null=True, help_text="Gametypes for this map, example: DM")
  min_players = models.IntegerField(default=0, help_text="Number of players required for this map to be fun")
  max_players = models.IntegerField(default=64, help_text="Number of players when map becomes no more fun")
  exclude = models.BooleanField(default=False, help_text="Don't use this map in the map rotation, ignoring votes")
  images = models.ManyToManyField(Image, help_text="Images associated with this map, levelshots, topview, etc")
  last_played = models.DateTimeField(null=True,blank=True)

  def update_last_played(self):
    self.last_played = datetime.datetime.now()
    self.save()

  def save(self, *args, **kwargs):
      log.debug('saving map %s' % self.name)
      super(Map, self).save(*args, **kwargs)

      if not self.images.all():
        self.add_images()
  
  def add_images(self):
      log.debug('adding images')
      self.images.add(Image.objects.get_or_create(name=self.name, type='topview')[0])
      self.images.add(Image.objects.get_or_create(name=self.name, type='levelshot')[0])
      self.save()

  def vote_sum(self):
    return self.raw_votes.aggregate(Sum('vote'))

  def vote(self, vote, player):
    print player
    player = Player.objects.get(name=player)
    (v, new) = Vote.objects.get_or_create(player=player, map=self)
    v.vote = vote
    v.save()
    return v

  @classmethod
  def add_missing_images(cls):
    maps = cls.objects.all()
    jobs = [gevent.spawn(map.add_images) for map in maps]

    gevent.joinall(jobs, timeout=2)

    log.debug('%s maps processed' % len(jobs))
    

  def __unicode__(self):
    return self.nicename if self.nicename else self.name


class Vote(models.Model):
  player = models.ForeignKey(Player, related_name='votes')
  map = models.ForeignKey(Map, related_name='raw_votes')
  vote = models.IntegerField(default=1)
  
  def __unicode__(self):
    return "%s - %s - %s" % (self.player.name, self.map.name, self.vote)


register_task(Map.task_mapstats)
