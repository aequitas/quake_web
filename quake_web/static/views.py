from django.template import RequestContext
from django.shortcuts import render_to_response

def live_view(request):
    return render_to_response('live_view.html', {}, context_instance=RequestContext(request))
