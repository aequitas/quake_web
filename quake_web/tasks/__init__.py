from django.core.signals import request_started

import gevent 

import logbook
log = logbook.Logger()

tasklist = []

def register_task(task):
  tasklist.append(task)

def start_task_worker(*args, **kwargs):
  request_started.disconnect(start_task_worker)
  log.debug('task worker start')
  for task in tasklist:
    log.debug('spawning task %s' % task)
    gevent.spawn(task)

  log.debug('done spawning tasks')

request_started.connect(start_task_worker)

import tasks
