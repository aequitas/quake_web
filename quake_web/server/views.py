from django.utils import simplejson
from django.http import HttpResponse
from django.views.generic import ListView, DetailView, TemplateView

class ServerView(TemplateView):
  template_name = 'server.html'
