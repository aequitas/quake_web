from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
import gevent
import logbook
import sys

from django.conf import settings
from server.quake import player_info, player_list

class Command(BaseCommand):
    help = 'run rcon command on server'
    option_list = BaseCommand.option_list + (
        make_option('-q',action='store_false',dest='verbose', default=True,help="don't show debug"),
    )

    def handle(self, *args, **options):
        if not args[0] in player_list():
            print 'Player not online'
        print player_info(args[0])
