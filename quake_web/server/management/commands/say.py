from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
import gevent
import logbook
import sys

from django.conf import settings
from server.quake import rcon_command

class Command(BaseCommand):
    help = 'Say something ingame'
    option_list = BaseCommand.option_list + (
        make_option('-q',action='store_false',dest='verbose', default=True,help="don't show debug"),
    )

    def handle(self, *args, **options):
        what = args[0]
        r = rcon_command('say %s' % what)
        if options['verbose']:
            print r
