#!/var/www/quake/virtualenv/bin/python
import pyquake3

from django.conf import settings

host = settings.QUAKEHOST
port = settings.QUAKEPORT
passwd = settings.QUAKERCONPASSWD

def rcon_command(cmd):
    a = pyquake3.Administrator(host, port, passwd)
    return a.rcon_command(cmd)

def command(cmd):
    a = pyquake3.Guest(host, port, passwd)
    return a.rcon_command(cmd)

def server_stats():
    """give live in game info for the server. Eg. map,"""
    a = pyquake3.Guest(host, port, passwd)
    return None

def player_list():
    """list all player names for online players"""
    a = pyquake3.Guest(host, port, passwd)
    a.update()
    return [player.name for player in a.players]

def player_stats(playername):
    """give live in game statistics on the player. Eg. name, frags, ping, team, etc"""
    a = pyquake3.Administrator(host, port, passwd)
    a.update()

    player = [player for player in a.players if player.name == playername]

    if len(player) == 1:
        try:
            stats = ['frags', 'ping', 'name']

            p = dict()
            for stat in stats:
                p[stat] = getattr(player[0],stat)

            return p
        except None:
            print 'error'
            return None
    else:
        return None
    
def players_stats():
    """give live in game statistics on all players. Eg. name, frags, ping, team, etc"""
    a = pyquake3.Administrator(host, port, passwd)
    a.update()

    p = dict()
    if len(a.players):
        for player in a.players:
            try:
                stats = ['frags', 'ping', 'name']

                p[player.name] = dict()
                for stat in stats:
                    p[player.name][stat] = getattr(player,stat)

            except None:
                print 'error'
                return None
        return p
    else:
        print 'no players online'
        return None

def player_info(playername):
    """give all info known on the server about this player. Eg. name, frags, slot, guid, model, etc"""
    a = pyquake3.Administrator(host, port, passwd)
    a.update()
    a.rcon_update()
    player = [player for player in a.players if player.name == playername]
    if len(player) == 1:
        try:
            a.rcon_dumpuser(player[0])
            return player # TODO convert to dict
        except None:
            print 'error'
            return None
    else:
        return None

def raw_server_dump():
    a = pyquake3.Administrator(host, port, passwd)
    a.update()
    a.rcon_update()
    a.rcon_dumpuser_all()

    for x in dir(a):
        if x == 'variables':
            for y in a.variables:
                print "%20s %s" % (y,a.variables[y])
        elif not '__' in x:
            print "%20s %s" % (x,getattr(a, x))

def raw_player_dump():
    a = pyquake3.Administrator(host, port, passwd)
    a.update()
    a.rcon_update()
    a.rcon_dumpuser_all()

    for player in a.players:
        for x in dir(player):
            if x == 'variables':
                for y in player.variables:
                    print "%20s %s" % (y,player.variables[y])
            elif not '__' in x:
                print "%20s %s" % (x,getattr(player, x))
    
