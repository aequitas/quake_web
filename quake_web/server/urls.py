from django.conf.urls import patterns, url

import views

urlpatterns = patterns('',
    url(r'^live/?$', views.ServerView.as_view(), 'index'),
)
