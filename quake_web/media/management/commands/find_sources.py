from django.core.management.base import BaseCommand, CommandError
from media.models import Image
from optparse import make_option
import gevent
import logbook
import sys

class Command(BaseCommand):
    help = 'Trigger source search for images'
    option_list = BaseCommand.option_list + (
        make_option('-q',action='store_false',dest='verbose', default=True,help="don't show debug"),
    )

    def handle(self, *args, **options):
      loghandler = logbook.NestedSetup()

      if not options['verbose']:
        loghandler.objects.append(logbook.NullHandler())

      loghandler.objects.append(logbook.StreamHandler(sys.stdout, bubble=False, level=logbook.INFO))

      with loghandler.threadbound():
          Image.find_sources()
        
