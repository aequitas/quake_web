from django.contrib import admin
from models import *

admin.site.register(Image, admin.ModelAdmin)
admin.site.register(ImageSource, admin.ModelAdmin)
