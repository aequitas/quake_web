from django.db import models
from django.db import models
from django.conf import settings
import gevent
import unittest
import os.path
import glob
import requests
import hashlib
from StringIO import StringIO
from PIL import Image as PILImage

import logbook
log = logbook.Logger()

imagetypes = [
  ('levelshot','levelshot'),
  ('topview','topview'),
  ('playermodel','playermodel'),
]


class ImageSource(models.Model):
  url = models.CharField(max_length=1000, help_text="url where image can be found with, %s for name, example: http://example.com/topview/%s.jpg")
  notfoundhash = models.CharField(max_length=100,null=True,blank=True, help_text="MD5 hash of image returned when image not found")
  type = models.CharField(max_length=100, choices=imagetypes, help_text="Type of image, example: topview")

  def get_image(self, name):
    url = self.url % name
    log.debug('trying to get image %s from url %s' % (name, url))
    try:
      r = requests.get(url)
    except:
      log.error('failed to perform get requests')
      return False

    log.debug(r.status_code)
    if r.status_code == 200:
      if self.notfoundhash:
        m = hashlib.md5()
        m.update(r.content)
        md5 = m.hexdigest()
        log.debug('comparing md5 %s to notfound hash %s' % (md5, self.notfoundhash))
        if md5 == self.notfoundhash:
          log.debug('md5 %s does matches notfound hash %s' % (md5, self.notfoundhash))
          return False

      ext = url.split('.')[-1]
      imageurl = os.path.join('images',self.type,'%s.%s' % (name,ext))
      imagepath = os.path.join(settings.MEDIA_ROOT,imageurl)
      log.debug('storing %s bytes image as %s' % (len(r.text),imagepath))

      with open(imagepath,'w') as f:
        f.write(r.content)

      return imageurl

    return False

  def __unicode__(self):
    return "%s - %s" % (self.type, self.url)


class Image(models.Model):
  name = models.CharField(max_length=100, help_text="Name of the image, example: sarge")
  type = models.CharField(max_length=100, choices=imagetypes, help_text="Type of image, example: topview")
  url = models.CharField(max_length=1000, blank=True, null=True, help_text="Where is the image to be found")
  source = models.CharField(max_length=1000, blank=True, null=True, help_text="Where was the image found")

  def save(self, *args, **kwargs):
      if not self.pk:
        gevent.spawn(self.find_source)
      super(Image, self).save(*args, **kwargs)

  def find_source(self, force=False):
    log.debug('finding sources for %s, force:%s' % (self, force))
    if (not self.url) or force:
      images = glob.glob(os.path.join(settings.MEDIA_ROOT,'images',self.type,'%s.*' % self.name.replace('/','_')))
      log.debug(images)
      if images:
        log.info('found: %s' % images)
        self.url = images[0][len(settings.MEDIA_ROOT)+1:]
        self.save()
        return self.url
      else:
        log.debug('image not available locally, search sources to find & download it')
        for source in ImageSource.objects.filter(type=self.type):
          imageurl = source.get_image(self.name)
          if imageurl:
            log.info('found %s' % imageurl)
            self.url = imageurl
            self.save()
            return self.url
      return False

  def get_url(self):
    if self.url:
      return settings.MEDIA_URL + self.url

    return '/media/images/empty.png'

  @classmethod
  def find_sources(cls, force=False):
    images = cls.objects.filter(models.Q(url=None) | models.Q(url=''))
    jobs = [gevent.spawn(image.find_source, force) for image in images if not image.url or force]

    gevent.joinall(jobs, timeout=15)

    log.debug('%s image sources searches' % len(jobs))
    log.debug('%s image sources found' % len([job for job in jobs if job.value]))

  def __unicode__(self):
    return "%s - %s" % (self.type, self.name)
