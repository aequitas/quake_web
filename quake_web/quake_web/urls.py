from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'static.views.live_view'),
    url(r'^maps/', include('maps.urls')),
    url(r'^players/', include('players.urls', namespace='player',app_name='players')),
    url(r'^games/', include('games.urls')),
    #url(r'^server/', include('server.urls')),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
