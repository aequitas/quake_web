from django.conf import settings

if settings.DEBUG:
  from gevent import monkey
  monkey.patch_all()
